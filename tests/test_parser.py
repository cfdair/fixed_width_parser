#!/usr/bin/env python3
# encoding: utf-8
"""Run all the tests."""

# Third Party Imports
import pytest

# First Party Imports
from fixed_width_parser import parser


def test_parser_sum_of_offsets_greater_than_line_length() -> None:
    """Should run normally without error."""

    with pytest.raises(RuntimeError):
        parser.parse("asd", [1])


def test_parser_sum_of_offsets_less_than_line_length() -> None:
    """Should run normally without error."""

    with pytest.raises(RuntimeError):
        parser.parse("asd", [1, 2, 1, 5])


def test_parser_should_successfully_parse() -> None:
    """Should run normally without error."""

    response = parser.parse("asdgf", [1, 2, 1, 1])
    expected = "a,sd,g,f"
    assert response == expected


def test_parser_should_strip_space_from_start_of_field() -> None:
    """Should run normally without error."""

    response = parser.parse("a dgf", [1, 2, 1, 1])
    expected = "a,d,g,f"
    assert response == expected


def test_parser_should_strip_space_from_end_of_field() -> None:
    """Should run normally without error."""

    response = parser.parse("as gf", [1, 2, 1, 1])
    expected = "a,s,g,f"
    assert response == expected


def test_parser_should_leave_space_within_field() -> None:
    """Should run normally without error."""

    response = parser.parse("as dgf", [1, 3, 1, 1])
    expected = "a,s d,g,f"
    assert response == expected


def test_parser_should_handle_non_ascii_characters() -> None:
    """Should run normally without error."""

    response = parser.parse("片仮名", [1, 2])
    expected = "片,仮名"
    assert response == expected


def test_parser_should_handle_whitespace_inputs() -> None:
    """Should run normally without error."""

    response = parser.parse("   ", [1, 2])
    expected = ","
    assert response == expected


def test_parser_should_handle_empty_inputs() -> None:
    """Should run normally without error."""

    response = parser.parse("", [])
    expected = ""
    assert response == expected
