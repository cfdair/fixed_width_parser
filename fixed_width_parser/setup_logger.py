#!/usr/bin/env python3
# encoding: utf-8
"""Load the logger object."""

# Standard Library Imports
import os
import sys
import time
import logging
from logging import Logger


class UTCFormatter(logging.Formatter):
    """Set the time in the logs as UTC."""

    converter = time.gmtime


FORMATTER = UTCFormatter(
    "%(asctime)s - %(levelname)s — %(name)s" " — %(funcName)s:%(lineno)d — %(message)s"
)


def get(name: str) -> Logger:
    """Initialize the LOGGER class.

    Args:
        name (str): The name of the LOGGER, usually the name of the file that calls this method.

    Returns:
        Logger: A logging object
    """
    logger = logging.getLogger(f"fixed_width_parser.{name}")
    logger.setLevel(logging.INFO)
    logger.addHandler(get_console_handler())
    return logger


def get_console_handler() -> logging.StreamHandler:
    """Get the logger handler for the console.

    Returns:
        logging.StreamHandler: The stream handler
    """
    log_level = os.environ.get("LOGLEVEL", "INFO").upper()
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(FORMATTER)
    handler.setLevel(log_level)
    return handler
