#!/usr/bin/env python3
# encoding: utf-8
"""Load the lines of a fixed width file."""

# Standard Library Imports
from typing import Iterator

# Relative Imports
from . import setup_logger

LOGGER = setup_logger.get(__name__)


def load_lines(path: str, encoding: str = "windows-1252") -> Iterator[str]:
    """Load the lines of a fixed width file.

    The encoding is also provided and defaults to windows-1252.

    Args:
        path (str): The relative path to the file to load into memory.
        encoding (str): The encoding of the file.
            Defaults to windows-1252.

    Yields:
        str: A generator that returns the lines.
    """
    LOGGER.info(f"Loading lines from file {path} with {encoding} encoding...")
    with open(path, encoding=encoding) as f:
        for line in f.readlines():
            yield line.replace("\n", "")
