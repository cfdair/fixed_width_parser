#!/usr/bin/env python3
# encoding: utf-8
"""Parse a fixed width line."""

# Standard Library Imports
from typing import List

# Relative Imports
from . import setup_logger

LOGGER = setup_logger.get(__name__)


def parse(line: str, offsets: List[int]) -> str:
    """Parse a fixed width line according to provided offsets.

    Args:
        line (str): The fixed width line to parse.
        offsets (List[str]): The list of offsets.

    Returns:
        str: The csv values.

    Raises:
        RuntimeError: If there is a mismatch in the line and the offsets.
    """
    LOGGER.debug("Parsing line...")
    slices = []
    original_line = line
    for offset in offsets:
        if line == "":
            raise RuntimeError(
                f"The line '{original_line}'(size ="
                f" {len(original_line)}) is too short for the offsets "
                f"{offsets}(size = {sum(offsets)})."
            )
        slices.append(line[:offset].strip())
        line = line[offset:]
    if line != "":
        raise RuntimeError(
            f"The line '{original_line}'(size ="
            f" {len(original_line)}) is too long for the offsets "
            f"{offsets}(size = {sum(offsets)})."
        )
    return ",".join(slices)
