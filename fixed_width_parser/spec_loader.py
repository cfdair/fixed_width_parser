#!/usr/bin/env python3
# encoding: utf-8
"""A function to load the spec."""

# Standard Library Imports
import json
from typing import Any, Dict

# Relative Imports
from . import setup_logger

LOGGER = setup_logger.get(__name__)


def load(spec_path: str) -> Dict[str, Any]:
    """Load the spec of the fixed width file.

    Args:
        spec_path(str): The relative path to the spec.

    Returns:
        Dict[str, Any]: The spec.
    """
    LOGGER.info("Loading spec...")
    with open(spec_path) as f:
        spec = json.loads(f.read())
        spec["Offsets"] = [int(i) for i in spec["Offsets"]]
        return spec
