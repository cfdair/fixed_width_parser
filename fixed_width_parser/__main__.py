#!/usr/bin/env python3
# encoding: utf-8
"""Execute the main function of the package."""

# Relative Imports
from . import parser, spec_loader, setup_logger, fw_file_loader

LOGGER = setup_logger.get(__name__)

SPEC_PATH = "fixed_width_parser/specs/spec.json"
FIXED_LENGTH_FILE_PATH = "fixed_width_test_file.txt"
CSV_OUTPUT = "fixed_width_test_file.csv"


def __main__(spec_path: str, fixed_length_file_path: str, output_path: str) -> None:
    """Parse a fixed width file into a csv file.

    A fixed width file spec is also required.

    Args:
        spec_path (str): The relative path to the spec.
        fixed_length_file_path (str): The relative path to the fixed length file.
        output_path (str): The relative path to the output.
    """
    LOGGER.info("Beginning parser...")
    spec = spec_loader.load(spec_path)
    with open(output_path, "w", encoding="utf-8") as f:
        for line in fw_file_loader.load_lines(fixed_length_file_path):
            parsed_line = parser.parse(line, spec["Offsets"])
            f.write(parsed_line + "\n")
    LOGGER.info(f"Output written to {output_path}...")
    LOGGER.info("Complete!")


if __name__ == "__main__":
    __main__(SPEC_PATH, FIXED_LENGTH_FILE_PATH, CSV_OUTPUT)
