# -------------------------------------------------------------------------
#
# Docker based make targets
#
# -------------------------------------------------------------------------

all: build_dev test build run clean

build:
	@echo "Building main image..."
	docker build . -t samc/fixed_width_parser

run: build
	@echo "Running main function..."
	docker run --rm -it samc/fixed_width_parser

build_dev:
	@echo "Building development image..."
	docker build . -f Dockerfile.test -t samc/fixed_width_parser_dev

test: build_dev
	@echo "Running tests..."
	docker run --rm -it samc/fixed_width_parser_dev python -m pytest

reformat: build_dev
	@echo 'Reformatting with isort...'
	docker run --rm -it samc/fixed_width_parser_dev python -m isort --settings-path pyproject.toml fixed_width_parser tests
	@echo 'Reformatting with black...'
	docker run --rm -it samc/fixed_width_parser_dev python -m black . --config=pyproject.toml

lint: build_dev
	@echo 'Running isort...'
	docker run --rm -it samc/fixed_width_parser_dev python -m isort --settings-path pyproject.toml --check-only fixed_width_parser tests
	@echo 'Running black...'
	docker run --rm -it samc/fixed_width_parser_dev python -m black . --check --diff --config=pyproject.toml
	@echo 'Running flake8...'
	docker run --rm -it samc/fixed_width_parser_dev python -m flake8 . --config=.flake8
	@echo 'Running pylint...'
	docker run --rm -it samc/fixed_width_parser_dev python -m pylint --rcfile=pyproject.toml fixed_width_parser tests
	@echo 'Running pydocstyle...'
	docker run --rm -it samc/fixed_width_parser_dev python -m pydocstyle . --config=setup.cfg
	@echo 'Running mypy...'
	docker run --rm -it samc/fixed_width_parser_dev python -m mypy fixed_width_parser tests --config-file setup.cfg
	@echo 'Running darglint...'
	docker run --rm -it samc/fixed_width_parser_dev darglint **/*.py

clean:
	@echo "Removing all samc/fixed_width_parser_dev images..."
	docker image ls samc/fixed_width_parser_dev -q | xargs -I {} docker rmi -f {}
	@echo "Removing all samc/fixed_width_parser images..."
	docker image ls samc/fixed_width_parser -q | xargs -I {} docker rmi -f {}
	docker images -f "dangling=true" -q | xargs -I {} docker rmi -f {}
