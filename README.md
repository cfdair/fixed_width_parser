# Fixed Width Parser

## Introduction
This is a solution to the problem 1 of the Latitude Financial Services
job application process.

The problem relates to parsing a fixed_width_file that is windows-1252 encoded
to output a csv file that is utf-8 encoded.

### Tools
The most prominent high-level tools used to solve this problem were
Python, Make, Poetry, Docker.

### Approach
There is a default python project structure and approach that was employeed for
this problem.

This involves a standard Python project, with dependencies defined using poetry,
with tests written with pytest, dev and prod environments defined and executed
in Docker, and commands defined in a Makefile.

There are also linters as defined in the make file. These are isort,
black, flake8, pylint, pydocstyle, mypy, darglint.

There is a generated fixed_width_test_file.txt in the base directory of the
repository. This is windows-1252 encoded.

The commands to parse this file and generate a csv are provided below.

### Getting started
To execute the parser on the test file, execute:
```bash
make run
```

This will build the prod docker image and then run the main function
within the docker image.

## Development

To run the formatters, execute:
```bash
make reformat
```

This will build the dev docker image, and then run the formatters within
the dev docker image.

To run the linters, execute:
```bash
make lint
```

This will build the dev docker image, and then run the linters within
the dev docker image.

To run the tests, run:
```bash
  make test
```

This will build the dev docker image, and then run the tests within
the dev docker image.

## Clean up
To remove the docker imagesthat were created, execute:
```bash
make clean
```

This will search for all docker images associated with this project and remove
them, as well as any dangling images.
